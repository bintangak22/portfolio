import Table from "./generatedTable.js";

fetch("https://jsonplaceholder.typicode.com/users")
  .then((response) => response.json())
  .then((data) => {
    const table = new Table({
      columns: ["ID", "Name", "Username", "Email", "Address", "Company"],
      detail: data,
    });

    // const app = document.getElementById("app");
    table.renderById("app");
  })
  .then((error) => console.log(error));
