export default class Table {
  constructor(init) {
    this.init = init;
  }

  createHeader(data) {
    let open = `<thead><tr>`;
    let close = `</tr></thead>`;

    data.forEach((el) => {
      open += `<th>${el}</th>`;
    });
    return open + close;
  }

  createBody(data) {
    let open = "<tbody>";
    let close = "</tbody>";
    for (let i = 0; i < data.length; i++) {
      open += `<tr>
            <td>${data[i].id}</td>
            <td>${data[i].name}</td>
            <td>${data[i].username}</td>
            <td>${data[i].email}</td>
            <td>${data[i].address.street}, ${data[i].address.suite},${data[i].address.city}</td> 
            <td>${data[i].company.name}</td>
          </tr>`;
    }
    return open + close;
  }

  renderById(id) {
    let table =
      "<table class='table table-hover'>" +
      this.createHeader(this.init.columns) +
      this.createBody(this.init.detail) +
      "</table>";
    let element = document.getElementById(id);
    element.innerHTML = table;
  }

  //   loadTableFromId(id) {
  //     return document.getElementById(id);
  //   }
}
