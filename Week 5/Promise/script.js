// let url =
//   "https://newsapi.org/v2/top-headlines?country=id&q=&sortBy=publishedAt&apiKey=60f03c3fdb2a400d91aade0b50a5715f";

// fetch(url)
//   .then((response) => response.json())
//   .then((response) => console.log(response));

const apiKey = "0771ac00dcb0427e8d225a99b863c776";

const inputSearch = document.querySelector("#search-input");
const newsCards = document.querySelector(".news-container");

tampilanAwal();

//main
inputSearch.addEventListener("input", async function () {
  const filter = inputSearch.value.toLowerCase();

  try {
    newsCards.innerHTML = getMessage(true, "Loading...");
    const news = await getDataFromNewsAPI(filter);
    updateUICards(news);
  } catch (err) {
    newsCards.innerHTML = getMessage(false, err.message);
    console.log(err);
  }
});

async function tampilanAwal() {
  try {
    const newsData = await getDataFromNewsAPI("");
    updateUICards(newsData);
  } catch (err) {
    newsCards.innerHTML = getMessage(false, err.message);
    console.log(err);
  }
}

function getDataFromNewsAPI(keysearch) {
  return axios
    .get(
      `https://newsapi.org/v2/top-headlines?language=id&q=${keysearch}&sortBy=publishedAt&apiKey=${apiKey}`
    )
    .then((data) => data.data.articles);
}

function updateUICards(data) {
  let cards = "";
  data.forEach((news) => (cards += getNews(news)));

  if (data.length == 0) cards = getMessage(true, "Tidak ada berita");

  newsCards.innerHTML = cards;
}

//error(false) atau sukses tapi ada kondisi(true)
function getMessage(kondisi, pesan) {
  let isiClass;
  if (kondisi === true) {
    isiClass = `text-muted text-center`;
  } else {
    isiClass = `text-light text-center bg-danger`;
  }

  return `
    <div class="mt-2">
        <p class="${isiClass}">${pesan}</p>
    </div>`;
}

function getNews(data) {
  return `
        <div class="col-md-3 my-3">
            <div class="card">
                <img src="${data.urlToImage}" class="card-img-top" />
                <div class="card-body">
                    <h5 class="card-title">${data.title}</h5>
                    <p class="text-muted">
                        ${data.author} - ${data.publishedAt}
                    </p>
                    <p class="card-text">
                        ${data.description}
                    </p>
                    <a href="
                    ${data.url}" class="btn btn-primary" target="_blank">Read more..</a>
                </div>
            </div>
        </div>`;
}
